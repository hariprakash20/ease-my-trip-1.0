import "./App.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/Navbar/Navbar";
import { Routes, Route } from "react-router-dom";
import Homepage from "./components/Homepage/Homepage";
import FlightsFilter from "./components/FlightsFilter/FlightsFilter";
import Booking from "./components/Booking/Booking";
import BookingSuccessful from "./components/BookingSuccessful/BookingSuccessful";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/flights" element={<FlightsFilter />} />
        <Route path="/flights/booking/:id" element={<Booking />} />
        <Route path="/flights/bookingsuccessful" element={<BookingSuccessful />} />
      </Routes>
    </div>
  );
}

export default App;
