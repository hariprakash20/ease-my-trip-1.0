export const SEAT_STATUS = {
  AVAILABLE: "Available",
  SELECTED: "Selected",
  BOOKED: "Booked",
};

export const API_STATUS = {
  LOADING: "LOADING",
  LOADED: "LOADED",
  ERROR: "ERROR",
};
