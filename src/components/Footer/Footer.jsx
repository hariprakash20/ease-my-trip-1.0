import React from "react";

function Footer() {
  return (
    <div className="footer bg-dark">
      <div className="contact-info d-flex justify-content-between py-3 width-90 m-auto border-bottom border-light">
        <div className="references w-25">
          <div className="reference text-light p-1">OUR OFFERINGS</div>
          <div className="reference text-light p-1">QUICK LINKS</div>
          <div className="reference text-light p-1">POPULAR DESTINATION</div>
          <div className="reference text-light p-1">
            INTERNATIONAL DESTINATION
          </div>
          <div className="reference text-light p-1">POPULAR AIRLINE</div>
          <div className="reference text-light p-1">CONNECT WITH US</div>
          <div className="reference text-light p-1">OFFERS</div>
          <div className="reference text-light p-1">EMT INSIGHTS</div>
          <div className="reference text-light p-1">INVESTORS RELATION</div>
          <div className="reference text-light p-1">CURRENT OPENINGS</div>
        </div>
        <div className="services w-75 ms-5">
          <h5 className="text-light mb-3">
            Make your travel easy with a wide range of products and services.
          </h5>
          <div className="services-list d-flex justify-content-between">
            <div className="col1 font-size-80">
              <p className="text-light">Flights</p>
              <p className="text-light">Bus</p>
              <p className="text-light">Flight Status</p>
              <p className="text-light">Check PNR Status</p>
              <p className="text-light">Corporate Travel</p>
            </div>
            <div className="col2 font-size-80">
              <p className="text-light">Hotels</p>
              <p className="text-light">Cabs</p>
              <p className="text-light">Airlines</p>
              <p className="text-light">EMT PRO</p>
              <p className="text-light">Blog</p>
            </div>
            <div className="col3 font-size-80">
              <p className="text-light">Trains</p>
              <p className="text-light">Cruise</p>
              <p className="text-light">Airports</p>
              <p className="text-light">Activities</p>
              <p className="text-light">Flight Check-in</p>
            </div>
            <div className="col3 font-size-80">
              <p className="text-light">Holidays</p>
              <p className="text-light">Charters</p>
              <p className="text-light">Travel Guides</p>
              <p className="text-light">Travel Updates</p>
            </div>
          </div>
        </div>
      </div>
      <div className="about-us d-flex py-3 width-90 m-auto border-bottom border-light">
        <div>
          <img
            src="https://www.easemytrip.com/images/common/footer/EMTLogo_white.svg"
            alt=""
          />
          <p className="text-light">
            EaseMyTrip offers 'End to End' travel solutions including air
            tickets for more than 400 international and domestic airlines, hotel
            bookings for nearly 1 million hotels in India and abroad, cab
            booking with 4000+ cab operators, bus tickets with 2000+ bus
            operators, and railway tickets in India for all major cities.
          </p>
        </div>
        <div className="download-app">
          <div className="text-light">Download EaseMyTrip App</div>
          <div className="d-flex">
            <img
              className="m-2"
              src="https://www.easemytrip.com/images/common/footer/androidft.png"
              height="40"
              alt=""
            />
            <img
              className="m-2"
              src="https://www.easemytrip.com/images/common/footer/iosnovft.png"
              height="40"
              alt=""
            />
          </div>
        </div>
        <div className="qr-code flex-grow-1">
          <div className="text-light">SCAN QR CODE</div>
          <img
            className=""
            height="100"
            src="https://www.easemytrip.com/images/common/footer/ftnovqrcode.svg"
            alt=""
          />
        </div>
      </div>
      <div className="copyright d-flex py-3 width-90 m-auto justify-content-between">
        <p className="text-secondary p-3">Copyright © 2023 EaseMyTrip</p>
        <img height="40" src="https://www.easemytrip.com/images/common/footer/botft.png" alt="" />
      </div>
    </div>
  );
}

export default Footer;
