import React from "react";
import { Link } from "react-router-dom";
import airports from "../../data/airports.json";
import EachAirport from "../EachAirport/EachAirport";
import { useSelector, useDispatch } from "react-redux";
import {
  setSearchString,
  setDepartureDate,
  setReturnDate,
  setTravellers,
  setClass,
  swapAirports,
} from "../../redux/SelectedFilters/SelectedFiltersSlice";
import { changePassengerCount } from "../../redux/PassengerList/PassengerListSlice";

function SearchDetails() {
  const { selectedFilters } = useSelector((store) => {
    return store;
  });

  const dispatch = useDispatch();

  function handlePassengersCount(event) {
    dispatch(
      setTravellers({
        travellersType: event.target.value,
        operation: event.target.innerText,
      })
    );
    dispatch(
      changePassengerCount({
        travellersType: event.target.value,
        operation: event.target.innerText,
        count: selectedFilters.travellers[event.target.value]
      })
    );
  }

  let filteredAirports = airports.filter((each) => {
    if (
      each.city
        .toLowerCase()
        .startsWith(selectedFilters.searchString.toLowerCase())
    ) {
      return each;
    }
  });

  return (
    <div
      className="btn-group w-100 bg-light border border-dark"
      role="group"
      aria-label="Basic example"
    >
      <button
        type="button"
        className="btn btn-light btn-outline-secondary border-end-0"
        data-bs-toggle="modal"
        data-bs-target="#fromModal"
      >
        <div className="text-start">
          <p className="m-0">FROM</p>
          <h5 className="text-dark m-0">
            {selectedFilters.fromAirport.city
              ? selectedFilters.fromAirport.city
              : "Bangalore"}
          </h5>
          <p className="m-0">
            {selectedFilters.fromAirport.airportName
              ? selectedFilters.fromAirport.airportName
              : " [BLR] Bengaluru International Airport"}
          </p>
        </div>
      </button>
      <>
        {/* Button trigger modal */}
        {/* Modal */}
        <div className="modal position-absolute" id="fromModal" tabIndex={-1}>
          <div className="modal-dialog position-fixed">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="fromModalLabel">
                  <form className="d-flex">
                    <input
                      className="form-control me-2"
                      type="search"
                      placeholder="Search"
                      aria-label="Search"
                      id="from-search"
                      onChange={(event) => {
                        dispatch(setSearchString(event.target.value));
                      }}
                    />
                  </form>
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <div className="modal-body">
                {filteredAirports.map((each) => {
                  return (
                    <EachAirport key={each.id} airport={each} type="from" />
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </>
      <div
        className="swap border rounded-circle text-center align-center position-relative"
        onClick={() => dispatch(swapAirports())}
      >
        <i className="fa-solid fa-arrow-right-arrow-left text-center"></i>
      </div>

      <button
        type="button"
        className="btn btn-light btn-outline-secondary py-2 border-start-0"
        data-bs-toggle="modal"
        data-bs-target="#toModal"
      >
        <div className="text-start">
          <p className="m-0">To</p>
          <h5 className="text-dark m-0">
            {selectedFilters.toAirport.city
              ? selectedFilters.toAirport.city
              : "Goa"}
          </h5>
          <p className="m-0">
            {selectedFilters.toAirport.airportName
              ? selectedFilters.toAirport.airportName
              : "[GOI] Dabolim Goa International Airport"}
          </p>
        </div>
      </button>
      <>
        <div className="modal position-absolute" id="toModal" tabIndex={-1}>
          <div className="modal-dialog position-fixed">
            <div className="modal-content ms-5">
              <div className="modal-header">
                <h5 className="modal-title" id="fromModalLabel">
                  <form className="d-flex">
                    <input
                      className="form-control me-2"
                      type="search"
                      placeholder="Search"
                      aria-label="Search"
                      id="to-search"
                      onChange={(event) => {
                        dispatch(setSearchString(event.target.value));
                      }}
                    />
                  </form>
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <div className="modal-body">
                {filteredAirports.map((each) => {
                  return <EachAirport key={each.id} airport={each} type="to" />;
                })}
              </div>
            </div>
          </div>
        </div>
      </>

      <button
        type="button"
        className="btn btn-light btn-outline-secondary py-2"
      >
        <p>DEPATURE DATE</p>
        <input
          type="date"
          name=""
          id=""
          onChange={(event) => dispatch(setDepartureDate(event.target.value))}
          className="border-0 bg-light bg-opacity-25"
        />
      </button>
      <button
        type="button"
        className="btn btn-light btn-outline-secondary py-2"
      >
        <p>RETURN DATE</p>
        <input
          type="date"
          name=""
          id=""
          onChange={(event) => dispatch(setReturnDate(event.target.value))}
          className="border-0"
          disabled
        />
      </button>
      <button
        type="button"
        className="btn btn-light  btn-outline-secondary py-2 dropdown-toggle"
        id="dropdownMenuButton1"
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        TRAVELLER &amp; CLASS
        <h5 className="text-dark">
          {selectedFilters.travellers.adult +
            selectedFilters.travellers.children +
            selectedFilters.travellers.infant}
          Traveller(s)
        </h5>
        {selectedFilters.class}
      </button>

      <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
        <li>
          <a
            className="dropdown-item"
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
            }}
          >
            Adults
            <div
              className="btn-group mr-2 float-end"
              role="group"
              aria-label="Second group"
            >
              <button
                type="button"
                className="btn btn-light"
                value="adult"
                onClick={handlePassengersCount}
              >
                -
              </button>
              <button type="button" className="btn btn-light disabled">
                {selectedFilters.travellers.adult}
              </button>
              <button
                type="button"
                className="btn btn-light"
                onClick={handlePassengersCount}
                value="adult"
              >
                +
              </button>
            </div>
          </a>
        </li>
        <li>
          <a
            className="dropdown-item"
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
            }}
          >
            {" "}
            Children
            <div
              className="btn-group mr-2 float-end"
              role="group"
              aria-label="Second group"
            >
              <button
                type="button"
                className="btn btn-light"
                value="children"
                onClick={handlePassengersCount}
              >
                -
              </button>
              <button type="button" className="btn btn-light disabled">
                {selectedFilters.travellers.children}
              </button>
              <button
                type="button"
                className="btn btn-light"
                value="children"
                onClick={handlePassengersCount}
              >
                +
              </button>
            </div>
          </a>
        </li>
        <li>
          <a
            className="dropdown-item"
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
            }}
          >
            infant
            <div
              className="btn-group mr-2 float-end"
              role="group"
              aria-label="Second group"
            >
              <button
                type="button"
                className="btn btn-light"
                value="infant"
                onClick={handlePassengersCount}
              >
                -
              </button>
              <button type="button" className="btn btn-light disabled">
                {selectedFilters.travellers.infant}
              </button>
              <button
                type="button"
                className="btn btn-light"
                value="infant"
                onClick={handlePassengersCount}
              >
                +
              </button>
            </div>
          </a>
        </li>
        <li>
          <div className="class-selection-radio m-3">
            <>
              <div className="form-check ">
                <input
                  className="form-check-input align-middle"
                  type="radio"
                  name="exampleRadios"
                  id="exampleRadios1"
                  value="Economy"
                  onChange={(event) => dispatch(setClass(event.target.value))}
                />
                <label className="form-check-label" htmlFor="exampleRadios1">
                  Economy
                </label>
              </div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="exampleRadios"
                  id="exampleRadios2"
                  value="Prem.Economy"
                  onChange={(event) => dispatch(setClass(event.target.value))}
                />
                <label className="form-check-label" htmlFor="exampleRadios2">
                  Prem.Economy
                </label>
              </div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="exampleRadios"
                  id="exampleRadios3"
                  value="Business"
                  onChange={(event) => dispatch(setClass(event.target.value))}
                />
                <label className="form-check-label" htmlFor="exampleRadios3">
                  Business
                </label>
              </div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="exampleRadios"
                  id="exampleRadios3"
                  value="First"
                  onChange={(event) => dispatch(setClass(event.target.value))}
                />
                <label className="form-check-label" htmlFor="exampleRadios3">
                  First
                </label>
              </div>
            </>
          </div>
        </li>
        <button className="btn btn-primary width-90 ms-2">Done</button>
      </ul>
      <Link className="btn text-light search-btn pt-4" to="/flights">
        <h5 className="m-2">SEARCH</h5>
      </Link>
    </div>
  );
}

export default SearchDetails;
