import React from "react";
import FlightSearch from "../FlightSearch/FlightSearch";
import Guidelines from "../Guidelines/Guidelines";
import FAQ from "../FAQ/FAQ";
import Footer from "../Footer/Footer";

function Homepage() {
  return (
    <>
      <FlightSearch />
      <Guidelines />
      <FAQ />
      <Footer />
    </>
  );
}

export default Homepage;
