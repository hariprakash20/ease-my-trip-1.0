import React from "react";

function Guidelines() {
  return (
    <>
      <div className="guidelines">
        <div className="row1 d-flex">
          <div className="alert alert-warning w-50 m-2" role="alert">
            <b>Important Info:</b> To cancel/claim refund or reschedule/modify
            your booking. <a>click here</a>
          </div>
          <div className="alert alert-primary w-50 m-2" role="alert">
            <b>Travel Guide:</b>
            <u>
              Get latest information on airlines & airports guidelines,
              state-wise quarantine rules, travel checklists, web-checkin etc.
            </u>
          </div>
        </div>

        <div className="alert alert-success m-2" role="alert">
          <b> FREE Full Refund due to Medical Reasons: </b>EaseMyTrip brings an
          extraordinary offer in these tough times! Get a full refund on a
          domestic ticket in case you cancel it due to medical sickness. The
          Best Part - There are ZERO extra charges for this service.
        </div>
      </div>
    </>
  );
}

export default Guidelines;
