import React from "react";
import { useSelector, useDispatch} from 'react-redux';
import { updatePassengerDetails } from '../../redux/PassengerList/PassengerListSlice'

function TravellerDetails(props) {
  const each = props.each;

  const { passengerList } = useSelector((store)=>{
    return store;
  })

  const dispatch = useDispatch();

  function handleChange(event){
    let travellerType = '';
    if(each.includes('adult')){
      travellerType = "adult";
    }if(each.includes('children')){
      travellerType = 'children';
    }if(each.includes('infant')){
      travellerType = 'infant';
    }
    dispatch(updatePassengerDetails({
      travellerType : travellerType,
      traveller : each,
      inputField : event.target.name,
      value: event.target.value
    }))
  }

  function handleConfirm(event){
    event.preventDefault();
  }

  return (
    <>
      <div className="border shadow p-3 bg-body rounded m-3">
        {each}
        <form className="input-field d-flex justify-content-around">
          <div className="title m-3">
            <label htmlFor="basic-url" className="form-label">
              tilte
            </label>
            <select
              className="form-select"
              aria-label="Default select example"
              required
              name="title"
              onChange={handleChange}
            >
              <option>Title</option>
              <option value={"MR"}>MR</option>
              <option value={"MS"}>MS</option>
              <option value={"Mrs"}>Mrs</option>
            </select>
          </div>

          <div className="first-and-middle-name flex-grow-1 m-3">
            <label htmlFor="basic-url" className="form-label">
              First Name (& middle name if any)
            </label>
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                id="basic-url"
                required
                name='firstName'
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="last-name flex-grow-1 m-3">
            <label htmlFor="basic-url" className="form-label">
              Last Name
            </label>
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                id="basic-url"
                required
                name="lastName"
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="m-3">
            <label htmlFor="basic-url" className="form-label"></label>
            <button className="btn d-block btn-primary" type="submit" onClick={handleConfirm}>confirm</button>
          </div>
        </form>
        <div>
          <span className="p-4">Fare rules</span>
          <span className="p-4">Baggage</span>
        </div>
      </div>
    </>
  );
}

export default TravellerDetails;
