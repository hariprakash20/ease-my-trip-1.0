import React from "react";
import { Link } from 'react-router-dom'

function EachFlight(props) {
  let details = props.details;
  return (
    <>
    <div className="container each-flight bg-pure-white m-2 p-3">
    <div className="d-flex justify-content-around m-2">
        <img src={details.imageUrl} alt=""  width={70}/>
        <div className="airlines">{details.airlines}</div>
        <div className="from-city">
          <div><b>{details.depTime}</b></div>
          <div>{details.fromAirport}</div>
        </div>
        <div>
          <div>non-stop</div>
        </div>

        <div className="to-city">
          <div><b>{details.arrTime}</b></div>
          <div className="to">{details.toAirport}</div>
        </div>
        <div className="fare-details">
        <div className="fare text-danger fs-5">₹{details.fare}</div>
        <button className="btn border border-primary text-primary rounded-pill ">+ more fare</button>
        </div>
        <Link to={`booking/${details.id}`} className="btn book-btn bg-orange text-light">BOOK</Link>
      </div>
      <p className="m-3 bg-light-orange d-inline">Use Promo Code: BOOKNOW to get flat Rs.738 OFF on this flight</p>
    </div>
      
      
    </>
  );
}

export default EachFlight;
