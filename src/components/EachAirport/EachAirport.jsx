import React from "react";
import {setFromCity, setSearchString} from '../../redux/SelectedFilters/SelectedFiltersSlice'
import { useDispatch } from 'react-redux'

function EachAirport(props) {
  const airport = props.airport;
  const dispatch = useDispatch();
  return (
    <>
      <div className="select-airport border-bottom p-2" data-bs-dismiss="modal" onClick={()=>{  
        dispatch(setSearchString(""));
         dispatch(setFromCity({type:props.type ,data:airport}))
         } }>
        <div className="d-flex">
          <i className="fa-solid fa-plane m-3" style={{ color: "grey" }} />
          <div className="flex-grow-1">
            <h6 className="m-1">{airport.city}</h6>
            <p className="m-0 pb-2">{airport.airportName}</p>
          </div>
          <p className="pe-1">{airport.country}</p>
        </div>
      </div>
    </>
  );
}

export default EachAirport;
