import React from "react";
import { Link } from "react-router-dom";
import validator from "validator";
import { useState } from "react";
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { signUpValidate } from '../../redux/userInfo/userInfoSlice'

function Navbar() {

  const { userInfo } = useSelector((store)=>{
    return store;
  })
  const dispatch = useDispatch();

  const [user, setUser] = useState("");

  function handleSignIn() {
    if (userInfo.signInValidated) {
      setUser(userInfo.userName);
    }
  }

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light p-0 mx-5">
        <div className="container-fluid d-flex justify-content-around align-middle text-dark">
          {/* <a href="#"> */}
          <Link className="navbar-brand" to="/">
            <img
              src="https://www.easemytrip.com/images/brandlogo/emtlogo_new6.svg"
              alt=""
              height="50"
            />
          </Link>
          {/* </a> */}
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div
            className="collapse navbar-collapse align-middle"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav m-auto mb-2 mb-lg-0 d-flex align-items-center">
              <li className="nav-item border-end">
                <a className="nav-link active" aria-current="page" href="#">
                  <p className="font-size-m m-0 text-dark">FLIGHTS</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">HOTELS</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">FLIGHTS+HOTEL</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">BUS</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">CABS</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">CRUISE</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">TRAINS</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">HOLIDAYS</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">ACTIVITIES</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">GIFT CARD</p>
                </a>
              </li>
              <li className="nav-item border-end">
                <a className="nav-link" href="#">
                  <p className="font-size-m m-0 text-dark">CHARTERS</p>
                </a>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span className="font-size-m">more</span>
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="border-bottom">
                    <a className="dropdown-item" href="#">
                      CHARTERS
                    </a>
                  </li>
                  <li className="border-bottom">
                    <a className="dropdown-item" href="#">
                      ACTIVITIES
                    </a>
                  </li>
                  <li className="border-bottom">
                    <a className="dropdown-item" href="#">
                      GIFT CARD
                    </a>
                  </li>
                  <li className="border-bottom">
                    <a className="dropdown-item" href="#">
                      OFFERS
                    </a>
                  </li>
                  <li className="border-bottom">
                    <a className="dropdown-item" href="#">
                      CORPORATE TRAVEL
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="#">
                      AGENT LOGIN
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div className="mx-3 pt-0">
            <img
              src="https://www.easemytrip.com/emt-pro/img/emtpro-header-icon.svg"
              height="50"
              alt=""
            />
          </div>

          {user ? <b>{user}</b> : (
            <button
              type="button"
              className="btn myAccount"
              data-bs-toggle="modal"
              data-bs-target="#myAccountModal"
            >
              <i className="fa-regular fa-user" style={{ color: "#737373" }} />
              <span className="p-1">
                <b>My Account</b>
              </span>
              <i
                className="fa-solid fa-chevron-down"
                style={{ color: "#787878" }}
              />
            </button>
          )}

          <div
            className="modal float-right"
            id="myAccountModal"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="myAccountModelLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog me-0 mt-5 w-25" role="document">
              <div className="modal-content text-center">
                <i className="fa-regular fa-circle-user fs-1 p-1"></i>
                <button
                  type="button"
                  className="btn btn-primary m-2"
                  data-bs-toggle="modal"
                  data-bs-target="#exampleModal"
                >
                  LOGIN OR SIGNUP
                </button>
              </div>
            </div>
          </div>
          <>
            {/* Modal */}
            <div
              className="modal fade bg-blur"
              id="exampleModal"
              tabIndex={-1}
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <h5 className="m-3">
                    <b>Login or Create an account</b>
                  </h5>
                  <b className="m-3">Email ID or Mobile Number</b>
                  <div className="input-group ">
                    <input
                      type="text"
                      className="form-control m-3"
                      placeholder="Username"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                      onChange={(event) => dispatch(signUpValidate(event.target.value))}
                    />
                  </div>
                  <button
                    type="button"
                    className={`btn m-3 ${
                      userInfo.signInValidated ? "bg-orange" : "btn-secondary"
                    }`}
                    data-bs-dismiss="modal"
                    onClick={handleSignIn}
                  >
                    CONTINUE
                  </button>
                </div>
              </div>
            </div>
          </>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
