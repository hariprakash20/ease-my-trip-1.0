import React from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  addAirlines,
  removeAirlines,
} from "../../redux/SelectedFilters/SelectedFiltersSlice";
import EachFlight from "../EachFlight/EachFlight";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

const API_STATUS = {
  LOADING: "LOADING",
  LOADED: "LOADED",
  ERROR: "ERROR",
};

function FlightsFilter() {
  const dispatch = useDispatch();

  const [flights, setFlights] = useState({
    list: [],
    status: API_STATUS.LOADING,
  });

  const [airlines, setAirlines] = useState({
    "Air India": false,
    "Go Air": false,
    Vistara: false,
    SpiceJet: false,
    IndiGo: false,
    AirAsia: false,
  });

  useEffect(() => {
    axios
      .get("https://flights-api1-0.onrender.com/api/flights/")
      .then((response) => {
        setFlights({ list: response.data, status: API_STATUS.LOADED });
      })
      .catch((err) => {
        setFlights((prevState) => {
          return { ...prevState, status: API_STATUS.ERROR };
        });
      });
  }, []);

  const { selectedFilters } = useSelector((store) => {
    return store;
  });

  let filteredFlights = flights.list.filter((each) => {
    if (
      selectedFilters.fromAirport.city === each.fromAirport &&
      selectedFilters.toAirport.city === each.toAirport &&
      (selectedFilters.airlines.length === 0 ||
        selectedFilters.airlines.includes(each.airlines))
    )
      return each;
  });

  function handleFilter(event) {
    airlines[event.target.name]
      ? dispatch(removeAirlines(event.target.name))
      : dispatch(addAirlines(event.target.name));
    setAirlines((prevState) => {
      return {
        ...prevState,
        [event.target.name]: !airlines[event.target.name],
      };
    });
  }
  
  return (
    <>
      <div className="flightsPage">
        <div className="travel-details blue-gradient">
          <div className="containter width-90 m-auto">
            <>
              <div className="text-light">
                <>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="inlineRadioOptions-1"
                      id="inlineRadio1"
                    />
                    <label className="form-check-label" htmlFor="inlineRadio1">
                      ONE WAY
                    </label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="inlineRadioOptions-1"
                      id="inlineRadio2"
                    />
                    <label className="form-check-label" htmlFor="inlineRadio2">
                      ROUND TRIP
                    </label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="inlineRadioOptions-1"
                      id="inlineRadio3"
                    />
                    <label className="form-check-label" htmlFor="inlineRadio3">
                      MULTICITY
                    </label>
                  </div>
                </>
              </div>
            </>

            <div className="input-group mb-3">
              <input
                type="text"
                className="filters-input form-control m-2 bg-light-blue border-0 text-light"
                placeholder="Username"
                defaultValue={selectedFilters.fromAirport.city}
              />
              <input
                type="text"
                className="form-control m-2 bg-light-blue border-0 text-light"
                placeholder="Username"
                defaultValue={selectedFilters.toAirport.city}
              />
              <input
                type="date"
                className="form-control m-2 bg-light-blue border-0 text-light"
                placeholder="Username"
                defaultValue={selectedFilters.departureDate}
              />
              <input
                type="date"
                className="form-control m-2 bg-light-blue border-0"
                placeholder="Username"
                disabled
              />
              <input
                type="text"
                className="form-control m-2 bg-light-blue border-0"
                defaultValue={
                  selectedFilters.travellers.adults +
                  selectedFilters.travellers.children +
                  selectedFilters.travellers.infant +
                  "Traveller(s)"
                }
                disabled
              />
              <input
                type="text"
                className="form-control m-2 bg-light-blue border-0 text-light"
                defaultValue={selectedFilters.class}
              />
              <button className="btn btn-primary">Search</button>
            </div>

            <div className="radio-btn-group my-3 text-light">
              <>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="inlineRadioOptions-2"
                    id="inlineRadio1"
                    defaultValue="option1"
                  />
                  <label className="form-check-label" htmlFor="inlineRadio1">
                    Defence Forces
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="inlineRadioOptions-2"
                    id="inlineRadio2"
                    defaultValue="option2"
                  />
                  <label className="form-check-label" htmlFor="inlineRadio2">
                    Students
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="inlineRadioOptions-2"
                    id="inlineRadio3"
                    defaultValue="option3"
                    disabled=""
                  />
                  <label className="form-check-label" htmlFor="inlineRadio3">
                    Senior Citizens
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="inlineRadioOptions-2"
                    id="inlineRadio3"
                    defaultValue="option3"
                    disabled=""
                  />
                  <label className="form-check-label" htmlFor="inlineRadio3">
                    Doctors & Nurses
                  </label>
                </div>
              </>
            </div>
          </div>
        </div>
        <div className="main-container d-flex width-90 m-auto">
          <div className="filters w-25 bg-pure-white m-3">
            <h6 className="p-2 border-bottom">FILTER</h6>
            <p className="ps-2">
              <b>Popular Filters</b>
            </p>
            <div className="filters p-2">
              <>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="flexCheckDefault"
                    name="Air India"
                    onClick={handleFilter}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    Air India
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="checkbox-1"
                    defaultChecked=""
                    onClick={handleFilter}
                    name="Go Air"
                  />
                  <label className="form-check-label" htmlFor="checkbox-1">
                    Go Air
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="checkbox-2"
                    defaultChecked=""
                    name="Vistara"
                    onClick={handleFilter}
                  />
                  <label className="form-check-label" htmlFor="checkbox-2">
                    Vistara
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="checkbox-3"
                    defaultChecked=""
                    name="SpiceJet"
                    onClick={handleFilter}
                  />
                  <label className="form-check-label" htmlFor="checkbox-3">
                    Spice Jet
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="checkbox-4"
                    defaultChecked=""
                    name="IndiGo"
                    onClick={handleFilter}
                  />
                  <label className="form-check-label" htmlFor="checkbox-4">
                    Indi Go
                  </label>
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="checkbox-5"
                    defaultChecked=""
                    onClick={handleFilter}
                    name="AirAsia"
                  />
                  <label className="form-check-label" htmlFor="checkbox-5">
                    Air Asia
                  </label>
                </div>
              </>
            </div>
          </div>
          {flights.status === API_STATUS.LOADING ? (
            <><img src="https://flight.easemytrip.com/Content/img/loading-ban.gif" className="m-5 bg-light" width={250} height={250}/></>
          ) : (
            <div className="fights-list w-75">
              {filteredFlights.length ? (
                filteredFlights.map((each) => {
                  return <EachFlight key={each.id} details={each} />;
                })
              ) : (
                <>
                  <div
                    className="card flights-not-found mt-5"
                    style={{ width: "60%", margin: "auto" }}
                  >
                    <img
                      src="https://flight.easemytrip.com/Content/img/flight-not-found1.png"
                      className="card-img-top w-25 m-auto mt-3"
                      width="50"
                      alt="..."
                    />
                    <div className="card-body text-center">
                      <h5 className="card-title">Oops! No flights found</h5>
                      <p className="card-text">
                        There were no flights found for this date & route
                        combination
                      </p>
                      <Link to="/">
                        <div href="#" className="btn btn-primary">
                          Modify Search & Try Again
                        </div>
                      </Link>
                    </div>
                  </div>
                </>
              )}
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default FlightsFilter;
