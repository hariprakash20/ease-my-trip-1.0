import React from "react";
import { Link } from "react-router-dom";

function BookingSuccessful() {
  return (
    <div className="container text-center align-middle">
      <h1>BookingSuccessful</h1>
      <Link to="/" className="btn btn-primary">Back to Home</Link>
    </div>
  );
}

export default BookingSuccessful;
