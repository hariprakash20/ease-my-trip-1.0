import React from "react";

import SearchDetails from "../SearchDetails/SearchDetails";

export default function FlightSearch() {
  return (
    <div className="px-5 mb-4 rounded-3 blue-gradient">
      <div className="container-fluid py-3">
        <h6 className="display-5 text-center text-light fs-4 pt-3">
          <i className="fa-solid fa-plane mx-3" style={{ color: "#ffffff" }} />
          <span className="font-weight-500">Search Lowest Price</span>
        </h6>
        <div
          className="btn-group border-0 m-2 select-trip"
          role="group"
          aria-label="Basic radio toggle button group"
        >
          <input
            type="radio"
            className="btn-check border-0"
            name="btnradio"
            id="btnradio1"
            autoComplete="off"
            defaultChecked={true}
          />
          <label
            className="btn btn-outline-light border-0 rounded-pill "
            htmlFor="btnradio1"
          >
            One Way
          </label>
          <input
            type="radio"
            className="btn-check"
            name="btnradio"
            id="btnradio2"
            autoComplete="off"
          />
          <label
            className="btn btn-outline-light border-0 rounded-pill"
            htmlFor="btnradio2"
          >
            Round Trip
          </label>
          <input
            type="radio"
            className="btn-check"
            name="btnradio"
            id="btnradio3"
            autoComplete="off"
          />
          <label
            className="btn btn-outline-light border-0 rounded-pill"
            htmlFor="btnradio3"
          >
            Multi-City
          </label>
        </div>
        <SearchDetails />
        <div className="radio-btn-group my-3 text-light">
          <>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="inlineRadioOptions"
                id="inlineRadio1"
                defaultValue="option1"
              />
              <label className="form-check-label" htmlFor="inlineRadio1">
                Defence Forces
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="inlineRadioOptions"
                id="inlineRadio2"
                defaultValue="option2"
              />
              <label className="form-check-label" htmlFor="inlineRadio2">
                Students
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="inlineRadioOptions"
                id="inlineRadio3"
                defaultValue="option3"
                disabled=""
              />
              <label className="form-check-label" htmlFor="inlineRadio3">
                Senior Citizens
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="inlineRadioOptions"
                id="inlineRadio3"
                defaultValue="option3"
                disabled=""
              />
              <label className="form-check-label" htmlFor="inlineRadio3">
                Doctors & Nurses
              </label>
            </div>
            <button type="button" className="btn btn-light float-end bg-transparent text-light">Web Check-In</button>
          </>
        </div>
      </div>
    </div>
  );
}
