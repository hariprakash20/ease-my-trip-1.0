import { useEffect, useState, React } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";
import SeatSelection from "../SeatSelection/SeatSelection";
import { API_STATUS, SEAT_STATUS } from "../../data/stringConstants";
import TravellerDetails from "../TravellerDetails/TravellerDetails";

function Booking() {
  const { id } = useParams();
  const navigate = useNavigate();
  const { selectedFilters, passengerList } = useSelector((store) => {
    return store;
  });

  let totalValidation = true;

  Object.entries(passengerList.adult).map(([key, value]) =>{
    Object.values(value).map((each)=>{
      if(!each){
        totalValidation=false;
      }
    })
  })
  Object.entries(passengerList.children).map(([key, value]) =>{
    Object.values(value).map((each)=>{
      if(!each){
        totalValidation=false;
      }
    })
  })
  Object.entries(passengerList.infant).map(([key, value]) =>{
    Object.values(value).map((each)=>{
      if(!each){
        totalValidation=false;
      }
    })
  })

  const [flight, setFlight] = useState({
    data: {},
    status: API_STATUS.LOADING,
  });

  const [tab, setTab] = useState("Seats");

  function handleTabSwitch(event) {
    if (event.target.innerText !== tab) {
      setTab(event.target.innerText);
    } else {
      return;
    }
  }

  useEffect(() => {
    axios
      .get(`https://flights-api1-0.onrender.com/api/flights/${id}`)
      .then((response) => {
        setFlight({ data: response.data, status: API_STATUS.LOADED });
      })
      .catch((err) => {
        setFlight((prevState) => {
          return { ...prevState, status: API_STATUS.ERROR };
        });
      });
  }, []);

  let { seatsAvailabilityStatus } = flight.data;

  function handleBooking() {
    if(totalValidation){
      let noOfPassengers = passengerList.noOfPassengers;
    let noOfSelectedSeats = Object.values(seatsAvailabilityStatus).filter(
      (each) => {
        if (each === "Selected") {
          return each;
        }
      }
    ).length;

    if (noOfPassengers === noOfSelectedSeats) {
      let selectedToBooked = Object.fromEntries(
        Object.entries(flight.data.seatsAvailabilityStatus).map((each) => {
          return each[1] === SEAT_STATUS.SELECTED
            ? [each[0], SEAT_STATUS.BOOKED]
            : [each[0], each[1]];
        })
      );

      axios.put(`https://flights-api1-0.onrender.com/api/flights/${id}`, {
        seatsAvailabilityStatus: selectedToBooked,
      });
      navigate("/flights/bookingsuccessful");
    } else {
      return;
    }
    }
  }

  let total =
    selectedFilters.travellers.adult * flight.data.fare +
    selectedFilters.travellers.children * flight.data.fare +
    selectedFilters.travellers.infant * flight.data.fare;

  return (
    <>
      <div className="booking-page d-flex bg-light-blue">
        {flight.status === API_STATUS.LOADING ? (
          <>
            <img
              src="https://flight.easemytrip.com/Content/img/loading-ban.gif"
              className="m-5 bg-light"
              width={250}
              height={250}
            />
          </>
        ) : (
          <div className="container w-75 bg-light m-5 p-0 d-flex flex-column">
            <h3 className="bg-light-blue-gradient p-3">Flight Details</h3>
            <div className="container p-3 width-90">
              <div className="container border shadow p-3 bg-body rounded">
                <div className="container d-flex m-2">
                  <i
                    className="fa-solid fa-plane m-2"
                    style={{ color: "#000" }}
                  />
                  <h5>
                    {flight.data.fromAirport + " - " + flight.data.toAirport}
                  </h5>
                </div>
                <div className="container d-flex justify-content-around m-3">
                  <img src={flight.data.imageUrl} width={75} alt="" />
                  <h6>{flight.data.airlines}</h6>
                  <div>
                    <h5>{flight.data.depTime}</h5>
                    <p>{flight.data.fromAirport}</p>
                  </div>
                  <div>
                    <i
                      className="fa-solid fa-plane m-1 d-block text-center"
                      style={{ color: "#999" }}
                    />
                    <button className="btn btn-success bg-light text-success border-success rounded-pill">
                      REFUNDABLE
                    </button>
                  </div>
                  <div>
                    <h5>{flight.data.arrTime}</h5>
                    <p>{flight.data.toAirport}</p>
                  </div>
                </div>
                <div>
                  <span className="p-4">Fare rules</span>
                  <span className="p-4">Baggage</span>
                </div>
              </div>
            </div>

            <div className="container p-0">
              <h3 className="bg-light-blue-gradient p-3">
                <i className="fa-solid fa-user-group"></i>Travellers Details
              </h3>
              <div className="container p-3 width-90">ADULT</div>
              {Object.keys(passengerList.adult).map((each) => {
                return <TravellerDetails key={each} each={each} />;
              })}
              <div className="container p-3 width-90">CHILDREN</div>
              {Object.keys(passengerList.children).map((each) => {
                return <TravellerDetails key={each} each={each} />;
              })}
              <div className="container p-3 width-90">INFANTS</div>
              {Object.keys(passengerList.infant).map((each) => {
                return <TravellerDetails key={each} each={each} />;
              })}
            </div>

            <div className="container p-0">
              <div className="container p-3 width-90">
                <div className="container border shadow p-3 bg-body rounded">
                  <h4 className="text-primary">
                    <img
                      src="https://flight.easemytrip.com/Content/img/coverage-icon-v1.svg"
                      alt=""
                    />
                    FREE Medical Refund Policy
                  </h4>
                  <p>
                    Get full airline refund, if you cancel tickets due to
                    illness or sickness. This service is provided at sickness.
                    This service is provided at ZERO additional charges.
                  </p>
                  <div className="">
                    <>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault1"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="flexRadioDefault1"
                        >
                          Yes, I want to add Medical Refund Policy (FREE) to
                          this flight
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault2"
                          defaultChecked=""
                        />
                        <label
                          className="form-check-label"
                          htmlFor="flexRadioDefault2"
                        >
                          No, I don't wish to add Medical Refund Policy (FREE)
                          to this flight
                        </label>
                      </div>
                    </>
                  </div>
                </div>
              </div>
            </div>
            <div className="container p-0">
              <div className="container p-3 width-90">
                <div className="container border shadow p-0 bg-body rounded">
                  <div className="header d-flex bg-light-green">
                    <img
                      src="https://flight.easemytrip.com/m_content/img/f-icon-9.png"
                      alt=""
                      width={50}
                      height={50}
                      className=""
                    />
                    <div>
                      <h4>Good to Know</h4>
                      <p>Information you should know</p>
                    </div>
                  </div>
                  <div className="content">
                    <ul>
                      <li>
                        Airline Cancellation Fee is Rs 867 per passenger for
                        your selected flight on the sector Bangalore to Goa
                      </li>
                      <li>
                        Certify your health status through the Aarogya Setu app
                        or the self-declaration form at the airport
                      </li>
                      <li>
                        Remember to web check-in before arriving at the airport
                      </li>
                      <li>Face masks are compulsory</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="container p-0">
              <div className="container p-3 width-90">
                <div className="container border shadow p-3 bg-body rounded ">
                  <div className="sub-header border-bottom">
                    <ul className="nav nav-tabs">
                      <li className="nav-item">
                        <a
                          className={`nav-link ${
                            tab === "Seats" ? "active" : ""
                          } `}
                          aria-current="page"
                          onClick={handleTabSwitch}
                        >
                          <img
                            src="https://flight.easemytrip.com/Content/img/seatIcon.png"
                            alt=""
                          />
                          Seats
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className={`nav-link ${
                            tab === "Meal" ? "active" : ""
                          }`}
                          onClick={handleTabSwitch}
                        >
                          <img
                            src="https://flight.easemytrip.com/Content/img/MealIcon.png"
                            alt=""
                          />
                          Meal
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className={`nav-link ${
                            tab === "Baggage" ? "active" : ""
                          }`}
                          onClick={handleTabSwitch}
                        >
                          <img
                            src="https://flight.easemytrip.com/Content/img/BaggageIcon.png"
                            alt=""
                          />
                          Baggage
                        </a>
                      </li>
                    </ul>
                  </div>
                  {tab === "Seats" ? (
                    <div className="content d-flex m-3">
                      <div className="seating-details w-25">
                        <div className="flight-details d-flex m-3">
                          <img src={flight.data.imageUrl} alt="" width={90} />
                          <div className="details ms-3">
                            <h6>{flight.data.airlines}</h6>
                            <p>I5 753</p>
                          </div>
                        </div>
                        <div className="seat-allotment bg-light-grey border rounded-4 p-3">
                          {Object.keys(passengerList.adult).map((each) => {
                            return (
                              <p key={each}>
                                <span> {each} </span>{" "}
                                {passengerList.adult[each].seatNumber && (
                                  <span className="float-end badge bg-success">
                                    {" "}
                                    {passengerList.adult[each].seatNumber}
                                  </span>
                                )}
                              </p>
                            );
                          })}
                          {Object.keys(passengerList.children).map((each) => {
                            return (
                              <p key={each}>
                                <span> {each} </span>{" "}
                                {passengerList.children[each].seatNumber && (
                                  <span className="float-end badge bg-success">
                                    {" "}
                                    {passengerList.children[each].seatNumber}
                                  </span>
                                )}
                              </p>
                            );
                          })}
                          {Object.keys(passengerList.infant).map((each) => {
                            return (
                              <p key={each}>
                                <span> {each} </span>{" "}
                                {passengerList.infant[each].seatNumber && (
                                  <span className="float-end badge bg-success">
                                    {" "}
                                    {passengerList.infant[each].seatNumber}
                                  </span>
                                )}
                              </p>
                            );
                          })}
                        </div>
                      </div>
                      <SeatSelection
                        flight={flight}
                        setFlight={setFlight}
                      />
                    </div>
                  ) : (
                    <h1>Details Not available</h1>
                  )}
                </div>
              </div>
            </div>

            <button
              className={`btn w-50 m-auto my-3 ${totalValidation ? "bg-orange" : 'bg-secondary'}`}
              onClick={handleBooking}
            >
              CONTINUE BOOKING
            </button>
          </div>
        )}
        <div className="container w-25 bg-light m-5 p-0">
          <h4 className="bg-light-blue-gradient m-0 p-3">
            Price Summary
            <span className="fs-6 ms-3">
              <i className="fa-solid fa-person m-1">
                {selectedFilters.travellers.adult}
              </i>
              <i className="fa-solid fa-child-reaching m-1">
                {selectedFilters.travellers.children}
              </i>
              <i className="fa-solid fa-baby m-1">
                {selectedFilters.travellers.infant}
              </i>
            </span>
          </h4>
          {selectedFilters.travellers.adult && (
            <div className="container d-flex justify-content-between p-2 border-bottom">
              <span>Adults x{selectedFilters.travellers.adult}</span>
              <span>
                {(
                  selectedFilters.travellers.adult * flight.data.fare
                ).toString()}
              </span>
            </div>
          )}
          {selectedFilters.travellers.children ? (
            <div className="container d-flex justify-content-between p-2 border-bottom">
              <span>children x{selectedFilters.travellers.children}</span>
              <span>
                {(
                  selectedFilters.travellers.children * flight.data.fare
                ).toString()}
              </span>
            </div>
          ) : (
            <></>
          )}
          {selectedFilters.travellers.infant ? (
            <div className="container d-flex justify-content-between p-2 border-bottom">
              <span>infant x{selectedFilters.travellers.infant}</span>
              <span>
                {(
                  selectedFilters.travellers.infant * flight.data.fare
                ).toString()}
              </span>
            </div>
          ) : (
            <></>
          )}
          <h5 className="text-danger m-3">Grand Total : {total}</h5>
        </div>
      </div>
      <div className="footer"></div>
    </>
  );
}

export default Booking;
