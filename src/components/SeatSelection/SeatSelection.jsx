import React from "react";
import { SEAT_STATUS } from "../../data/stringConstants";
import { useSelector, useDispatch } from "react-redux";
import { updateSeat } from "../../redux/PassengerList/PassengerListSlice.js";

function SeatSelection({ flight, setFlight }) {
  let arr = [1, 2, 3, 4, 5, 6, 7, 8];

  let { seatsAvailabilityStatus } = flight.data;

  const dispatch = useDispatch();

  const { passengerList } = useSelector((store) => {
    return store;
  });

  function handleClick(event) {
    let selectedSeat = event.target.innerText;
    let noOfPassengers = passengerList.noOfPassengers;
    let noOfSelectedSeats = Object.values(seatsAvailabilityStatus).filter(
      (each) => {
        if (each === "Selected") {
          return each;
        }
      }
    ).length;
    let fillNext = Object.entries(passengerList.list).find(([key, value]) => {
      if (value === selectedSeat) {
        return key;
      }
    });
    if(!fillNext){
      fillNext = Object.entries(passengerList.list).find(([key, value]) => {
        if (value === "") {
          return key;
        }
      });
    }
    if(!fillNext){
      fillNext = Object.entries(passengerList.list)[noOfPassengers-1];
    }

    let travellerType = "";
      if (fillNext && fillNext[0].includes("adult")) {
        travellerType = "adult";
      }
      if( fillNext && fillNext[0].includes("children")){
        travellerType = 'children';
      }
      if( fillNext && fillNext[0].includes("infant")){
        travellerType = 'infant';
      }

    if (noOfPassengers > noOfSelectedSeats) {
      setFlight((prevState) => {
        if (
          prevState.data.seatsAvailabilityStatus[event.target.innerText] ===
          SEAT_STATUS.AVAILABLE
        ) {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              seatsAvailabilityStatus: {
                ...prevState.data.seatsAvailabilityStatus,
                [event.target.innerText]: SEAT_STATUS.SELECTED,
              },
            },
          };
        } else if (
          prevState.data.seatsAvailabilityStatus[event.target.innerText] ===
          SEAT_STATUS.SELECTED
        ) {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              seatsAvailabilityStatus: {
                ...prevState.data.seatsAvailabilityStatus,
                [event.target.innerText]: SEAT_STATUS.AVAILABLE,
              },
            },
          };
        } else if (
          prevState.data.seatsAvailabilityStatus[event.target.innerText] ===
          SEAT_STATUS.BOOKED
        ) {
          return { ...prevState };
        }
      }); 
      dispatch(
        updateSeat({
          seatStatus: seatsAvailabilityStatus[event.target.innerText],
          passenger: fillNext[0],
          seatNumber: event.target.innerText,
          travellerType: travellerType,
        })
      );
    } else if (noOfPassengers === noOfSelectedSeats) {
      setFlight((prevState) => {
        if (
          prevState.data.seatsAvailabilityStatus[event.target.innerText] ===
          SEAT_STATUS.AVAILABLE
        ) {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              seatsAvailabilityStatus: {
                ...prevState.data.seatsAvailabilityStatus,
                [fillNext[1]]: SEAT_STATUS.AVAILABLE,
                [event.target.innerText]: SEAT_STATUS.SELECTED,
              },
            },
          };
        } else if (
          prevState.data.seatsAvailabilityStatus[event.target.innerText] ===
          SEAT_STATUS.SELECTED
        ) {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              seatsAvailabilityStatus: {
                ...prevState.data.seatsAvailabilityStatus,
                [event.target.innerText]: SEAT_STATUS.AVAILABLE,
              },
            },
          };
        } else if (
          prevState.data.seatsAvailabilityStatus[event.target.innerText] ===
          SEAT_STATUS.BOOKED
        ) {
          return { ...prevState };
        }
      });
      dispatch(
        updateSeat({
          seatStatus: seatsAvailabilityStatus[selectedSeat],
          passenger: fillNext[0],
          seatNumber: selectedSeat,
          travellerType: travellerType,
        })
      );
    }
  }

  return (
    <>
      <div className="plane">
        <div className="cockpit">
          <img
            className="m-5"
            src="https://flight.easemytrip.com/Content/img/AirSeat/cockpit-top.png"
            alt=""
          />
        </div>
        <div className="seats">
          {seatsAvailabilityStatus &&
            arr.map((each) => {
              return (
                <div key={each}>
                  <div className="seat-row d-flex justify-content-between">
                    <div className="right-seats d-flex me-4">
                      <div
                        className={`seat m-1 rounded-3 text-light text-center ${
                          seatsAvailabilityStatus["A" + each] ===
                          SEAT_STATUS.BOOKED
                            ? "bg-secondary"
                            : seatsAvailabilityStatus["A" + each] ===
                              SEAT_STATUS.SELECTED
                            ? "bg-success"
                            : "bg-primary"
                        }`}
                        onClick={handleClick}
                      >
                        A{each}
                      </div>
                      <div
                        className={`seat m-1 rounded-3 text-light text-center ${
                          seatsAvailabilityStatus["B" + each] ===
                          SEAT_STATUS.BOOKED
                            ? "bg-secondary"
                            : seatsAvailabilityStatus["B" + each] ===
                              SEAT_STATUS.SELECTED
                            ? "bg-success"
                            : "bg-primary"
                        }`}
                        onClick={handleClick}
                      >
                        B{each}
                      </div>
                      <div
                        className={`seat m-1 rounded-3 text-light text-center ${
                          seatsAvailabilityStatus["C" + each] ===
                          SEAT_STATUS.BOOKED
                            ? "bg-secondary"
                            : seatsAvailabilityStatus["C" + each] ===
                              SEAT_STATUS.SELECTED
                            ? "bg-success"
                            : "bg-primary"
                        }`}
                        onClick={handleClick}
                      >
                        C{each}
                      </div>
                    </div>
                    <div className="left-seats d-flex ms-4">
                      <div
                        className={`seat m-1 rounded-3 text-light text-center ${
                          seatsAvailabilityStatus["D" + each] ===
                          SEAT_STATUS.BOOKED
                            ? "bg-secondary"
                            : seatsAvailabilityStatus["D" + each] ===
                              SEAT_STATUS.SELECTED
                            ? "bg-success"
                            : "bg-primary"
                        }`}
                        onClick={handleClick}
                      >
                        D{each}
                      </div>
                      <div
                        className={`seat m-1 rounded-3 text-light text-center ${
                          seatsAvailabilityStatus["E" + each] ===
                          SEAT_STATUS.BOOKED
                            ? "bg-secondary"
                            : seatsAvailabilityStatus["E" + each] ===
                              SEAT_STATUS.SELECTED
                            ? "bg-success"
                            : "bg-primary"
                        }`}
                        onClick={handleClick}
                      >
                        E{each}
                      </div>
                      <div
                        className={`seat m-1 rounded-3 text-light text-center ${
                          seatsAvailabilityStatus["F" + each] ===
                          SEAT_STATUS.BOOKED
                            ? "bg-secondary"
                            : seatsAvailabilityStatus["F" + each] ===
                              SEAT_STATUS.SELECTED
                            ? "bg-success"
                            : "bg-primary"
                        }`}
                        onClick={handleClick}
                      >
                        F{each}
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
}

export default SeatSelection;

