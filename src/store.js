import { configureStore } from "@reduxjs/toolkit";
import SelectedFiltersReducer from "./redux/SelectedFilters/SelectedFiltersSlice";
import userInfoReducer from "./redux/userInfo/userInfoSlice";
import passengerListReducer from "./redux/PassengerList/PassengerListSlice";

const store = configureStore({
    reducer: {
        selectedFilters: SelectedFiltersReducer,
        userInfo: userInfoReducer,
        passengerList: passengerListReducer
    }
})

export default store;
