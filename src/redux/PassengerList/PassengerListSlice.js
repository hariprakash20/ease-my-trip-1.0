import { createSlice } from "@reduxjs/toolkit";
import { SEAT_STATUS } from "../../data/stringConstants";

let initialState = {
  adult: {
    adult1: { seatNumber: "", title: "", firstName: "", lastName: "" },
  },
  children: {},
  infant: {},
  noOfPassengers: 1,
  list: {"adult1" : ""} ,
};

const passengerList = createSlice({
  name: "passengerList",
  initialState,
  reducers: {
    changePassengerCount: (state, { payload }) => {
      let newPassenger = payload.travellersType + (payload.count + 1);
      if (payload.operation === "+") {
        state[payload.travellersType][newPassenger] = {
          seatNumber: "",
          title: "",
          firstName: "",
          lastName: "",
        };
        state.noOfPassengers++;
        state.list[newPassenger] = "";
      } else if (
        (payload.operation === "-" &&
          payload.travellersType !== "adult" &&
          payload.count > 0) ||
        (payload.travellersType === "adult" && payload.count > 1)
      ) {
        delete state[payload.travellersType][
          payload.travellersType + payload.count
        ];
        state.noOfPassengers--;
        delete state.list[newPassenger];
      }
    },
    updateSeat: (state, {payload}) => {
      if(payload.seatStatus === SEAT_STATUS.AVAILABLE){
        state[payload.travellerType][payload.passenger].seatNumber = payload.seatNumber
        state.list[payload.passenger] = payload.seatNumber
      }else if(payload.seatStatus === SEAT_STATUS.SELECTED){
        state[payload.travellerType][payload.passenger].seatNumber = ""
        state.list[payload.passenger] = ""
      }else if(payload.seatStatus === SEAT_STATUS.BOOKED){
        return state
      }
    },
    updatePassengerDetails: (state, {payload}) =>{
      state[payload.travellerType][payload.traveller][payload.inputField] = payload.value;
    }
  },
});

export const { changePassengerCount, updateSeat, updatePassengerDetails } = passengerList.actions;

export default passengerList.reducer;
