import { createSlice } from "@reduxjs/toolkit";
import airports from "../../data/airports.json";

let initialState = {
  tripType: "One Way",
  searchString: "",
  fromAirport: airports[0],
  toAirport: airports[1],
  departureDate: "",
  returnDate: "",
  travellers: {
    adult: 1,
    children: 0,
    infant: 0,
  },
  class: "Economy",
  airlines: [],
};

const selectedFilters = createSlice({
  name: "selectedFilters",
  initialState,
  reducers: {
    setFromCity: (state, { payload }) => {
      if (payload.type === "from") {
        state.fromAirport = payload.data;
      } else if (payload.type === "to") {
        state.toAirport = payload.data;
      }
    },
    setSearchString: (state, { payload }) => {
      state.searchString = payload;
    },
    setDepartureDate: (state, { payload }) => {
      state.departureDate = payload;
    },
    setReturnDate: (state, { payload }) => {
      state.returnDate = payload;
    },
    setTravellers: (state, { payload }) => {
      if (payload.operation === "+") {
        state.travellers[payload.travellersType]++;
      } else if (
        (payload.operation === "-" &&
          payload.travellersType !== "adult" &&
          state.travellers[payload.travellersType] > 0) ||
        (payload.travellersType === "adult" &&
          state.travellers[payload.travellersType] > 1)
      ) {
        state.travellers[payload.travellersType]--;
      }
    },
    setClass: (state, { payload }) => {
      state.class = payload;
    },
    addAirlines: (state, {payload}) =>{
      state.airlines.push(payload);
    },
    removeAirlines: (state, {payload})=>{
      state.airlines = state.airlines.filter((each) => {
        if(each !== payload){
          return each;
        }
      })
    },
    swapAirports: (state) =>{
      let temp = state.fromAirport;
      state.fromAirport = state.toAirport;
      state.toAirport = temp;
    }

  },
});

export const {
  setFromCity,
  setSearchString,
  setDepartureDate,
  setReturnDate,
  setTravellers,
  setClass,
  addAirlines,
  removeAirlines,
  swapAirports
} = selectedFilters.actions;
export default selectedFilters.reducer;
