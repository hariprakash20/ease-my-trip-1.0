import { createSlice } from "@reduxjs/toolkit";
import validator from 'validator';

let initialState ={
    userName: '',
    signInValidated: false,
}

const userInfo = createSlice({
    name: "userInfo",
    initialState,
    reducers:{
        signUpValidate:(state, {payload})=>{
            state.userName = payload;
            if(validator.isEmail(payload) || (validator.isNumeric(payload) && payload.length=== 10) ){
                state.signInValidated = true;
            }
            else{
                state.signInValidated = false;
            }
        }
    }
})

export const {
    signUpValidate
}= userInfo.actions;

export default userInfo.reducer;